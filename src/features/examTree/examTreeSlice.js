import {createSlice} from '@reduxjs/toolkit';

export const examTreeSlice = createSlice({
    name: 'examTree',
    initialState: {
        tree: null,
    },
    reducers: {
        setExamTree: (state, action) => {
            state.tree = action.payload;
        },
    },
});

// Action creators are generated for each case reducer function
export const {setExamTree} = examTreeSlice.actions;

export default examTreeSlice.reducer;
