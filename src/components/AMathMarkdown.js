import React from 'react';
import {View} from 'react-native';
import MathView from 'react-native-math-view/src/fallback';
import Markdown, {MarkdownIt} from 'react-native-markdown-display';
import {useTheme} from 'styled-components/native';
import mdit from 'markdown-it';
import mk from 'markdown-it-katex';

const AMathMarkdown = ({children}) => {
    const theme = useTheme();
    const markdownStyles = {
        body: {
            flex: 1,
            color: theme.isDark
                ? theme.const.Colors.white
                : theme.const.Colors.black,
        },
        text: {
            fontSize: theme.const.Fonts.sizesInt.input,
        },
        ordered_list: {
            fontSize: theme.const.Fonts.sizesInt.input,
        },
        math_inline: {
            color: theme.isDark
                ? theme.const.Colors.white
                : theme.const.Colors.black,
        },
    };

    let md = new mdit();
    md.use(mk);

    const markdownItInstance = MarkdownIt({typographer: true, html: false}).use(mk);

    // {/*https://github.com/iamacup/react-native-markdown-display*/}
    // {/*https://github.com/ShaMan123/react-native-math-view*/}
    return (
        <>
            <Markdown
                style={markdownStyles}
                markdownit={markdownItInstance}
                rules={{
                    math_inline: (node, children, parent, styles) => {
                        return (
                            <MathView
                                key={node.key}
                                style={styles.math_inline}
                                math={node.content}
                                config={{ex: 10}}
                                resizeMode="contain"
                            />
                        );
                    },
                    math_block: (node, children, parent, styles) => {
                        return (
                            <MathView
                                resizeMode="contain"
                                key={node.key}
                                style={styles.math_inline}
                                math={node.content}
                                config={{ex: 12, inline: false}}
                            />
                        );
                    },
                }}>
                {children}
            </Markdown>
        </>
    );
};

export default AMathMarkdown;
