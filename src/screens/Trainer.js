// React
import React, {useEffect, useState} from 'react';

// Redux
import {useSelector} from 'react-redux';

// Navigation
import {useIsFocused, useNavigation} from '@react-navigation/native';

// Utils
import {num_word} from '../utils/enumerate';

// Styling
import styled from 'styled-components/native';

// Features
import {
    getExamSubjectNumUtr,
    getExamSubjectUtr,
    getThemeUtr,
} from '../features/tasks/tasksApi';
import {
    getExamSubjectNumStats,
    getExamThemeStats,
} from '../features/stats/statsApi';

// Components
import AProgressBar from '../components/AProgressBar';
import ASpinner from '../components/ASpinner';
import ABadge from '../components/ABadge';
import {ScrollContainer, ViewContainer} from '../components/Containers';
import {
    Text,
    TextH2,
    TextH3,
    TextLarge,
    TextSmall,
    TextTiny,
} from '../components/Typography';

/**
 * PROGRESS
 */

const ProgressSubTitleContainer = styled.View`
    justify-content: space-between;
    flex-direction: row;
    margin-top: 5px;
`;

const ProgressSubTitleText = styled(TextTiny)`
    color: ${props =>
        props.variant === 'light'
            ? props.theme.const.Colors.white
            : props.theme.const.Colors.darkGrayAccent};
`;

const TrainerProgressComponent = ({
    allQuestionsCount,
    solvedQuestions,
    percent,
    variant,
}) => {
    return (
        <>
            <AProgressBar value={(solvedQuestions / allQuestionsCount) * 100} />
            <ProgressSubTitleContainer>
                <ProgressSubTitleText variant={variant}>
                    Решено {solvedQuestions}\{allQuestionsCount} задач
                </ProgressSubTitleText>
                <ProgressSubTitleText variant={variant}>
                    {percent || 0}% готовности
                </ProgressSubTitleText>
            </ProgressSubTitleContainer>
        </>
    );
};

/**
 * THEME
 */

const ThemeContainer = styled.TouchableOpacity`
    padding-horizontal: 15px;
    padding-top: 20px;
`;

const ThemeTitle = styled(TextH3)`
    color: ${props => props.theme.const.Colors.darkGrayAccent};
`;

const ThemeComponent = ({theme, num, idx}) => {
    const [themeStats, setThemeStats] = useState();
    const navigation = useNavigation();
    const isFocused = useIsFocused();

    useEffect(() => {
        if (isFocused) {
            getExamThemeStats(theme.id).then(stats => {
                setThemeStats(stats);
            });
        }
    }, [isFocused]);

    const loadThemeUtr = () => {
        getThemeUtr(theme.id).then(utr => {
            navigation.navigate('Task', {utrId: utr.id});
        });
    };

    return (
        <ThemeContainer onPress={loadThemeUtr}>
            <ThemeTitle>
                {num}.{idx}. {theme.title}
            </ThemeTitle>
            <TrainerProgressComponent
                allQuestionsCount={theme.questions_count}
                solvedQuestions={themeStats ? themeStats.total_solved : 0}
                percent={themeStats ? themeStats.percent : 0}
            />
        </ThemeContainer>
    );
};

/**
 * NUM
 */

const ModalButtonContainer = styled.TouchableOpacity`
    width: 110px;
`;

const ModalButtonText = styled(Text)`
    background-color: ${props => props.theme.const.Colors.white};
    color: ${props => props.theme.const.Colors.darkGrayAccent};
    height: 30px;
    line-height: 30px;
    font-size: ${props => props.theme.const.Fonts.sizes.tiny};
    width: 110px;
    border-radius: 5px;
    text-align: center;
    overflow: hidden;
    margin-top: 10px;
`;

const ModalButton = ({isModalOpen, setIsModalOpen}) => {
    return (
        <ModalButtonContainer onPress={() => setIsModalOpen(!isModalOpen)}>
            <ModalButtonText>
                {isModalOpen ? 'Скрыть' : 'Подробнее'}
            </ModalButtonText>
        </ModalButtonContainer>
    );
};

const NumHeaderContainer = styled.TouchableOpacity`
    background-color: ${props => {
        if (props.solved) {
            return props.theme.const.Colors.blueAccent;
        } else {
            return props.theme.const.Colors.darkGrayAccent;
        }
    }};
    border-radius: 5px;
    padding: 15px;
`;

const NumHeaderSubTitle = styled(TextSmall)`
    color: ${props => props.theme.const.Colors.white};
`;

const NumHeaderTitle = styled(TextH2)`
    color: ${props => props.theme.const.Colors.white};
`;

const NumContainer = styled.View`
    background-color: ${props => props.theme.const.Colors.lightGrayAccent};
    border-radius: 5px;
    margin-bottom: 15px;
`;

const ThemesContainer = styled.View`
    padding-bottom: 15px;
`;

const NumHeader = ({num, title}) => {
    return (
        <>
            <NumHeaderSubTitle>Задание {num}</NumHeaderSubTitle>
            <NumHeaderTitle>{title}</NumHeaderTitle>
        </>
    );
};

const NumComponent = ({num, examTagId, subjectTagId}) => {
    const [isModalOpen, setIsModalOpen] = useState();
    const [numStats, setNumStats] = useState();
    const navigation = useNavigation();

    const solved = numStats ? numStats.total_solved !== 0 : false;

    const isFocused = useIsFocused();

    useEffect(() => {
        if (isFocused) {
            getExamSubjectNumStats(examTagId, subjectTagId, num.num).then(
                stats => {
                    setNumStats(stats);
                },
            );
        }
    }, [isFocused]);

    const loadNumUtr = () => {
        getExamSubjectNumUtr(examTagId, subjectTagId, num.num).then(utr => {
            navigation.navigate('Task', {utrId: utr.id});
        });
    };

    return (
        <NumContainer>
            <NumHeaderContainer solved={solved} onPress={loadNumUtr}>
                <NumHeader
                    num={num.num}
                    title={num.title ? num.title : num.themes[0].title}
                    solved={solved}
                />
                <TrainerProgressComponent
                    variant={'light'}
                    allQuestionsCount={num.all_questions_count}
                    solvedQuestions={numStats ? numStats.total_solved : 0}
                    percent={numStats ? numStats.percent : 0}
                />
                {num.themes.length > 1 && (
                    <ModalButton
                        isModalOpen={isModalOpen}
                        setIsModalOpen={setIsModalOpen}
                    />
                )}
            </NumHeaderContainer>
            {num.themes.length > 1 && isModalOpen && (
                <ThemesContainer>
                    {num.themes.map((theme, i) => (
                        <ThemeComponent
                            key={theme.id}
                            theme={theme}
                            num={num.num}
                            idx={i + 1}
                        />
                    ))}
                </ThemesContainer>
            )}
        </NumContainer>
    );
};

/**
 * SCREEN
 */

const TrainerStatsContainer = styled.View`
    margin-top: 10px;
`;

const TrainerStats = ({stats, examTitle}) => {
    if (stats) {
        if (stats.total_solved > 10) {
            return (
                <TrainerStatsContainer>
                    <TextH3>
                        Ты сдашь {examTitle ? examTitle + ' ' : false}на
                    </TextH3>
                    <TextLarge>
                        {stats.percent}{' '}
                        {num_word(stats.percent, ['балл', 'балла', 'баллов'])}
                    </TextLarge>
                </TrainerStatsContainer>
            );
        } else {
            return (
                <TrainerStatsContainer>
                    <TextH2>Продолжай решать!</TextH2>
                    <TextSmall>
                        Здесь появится прогноз твоего результата
                    </TextSmall>
                </TrainerStatsContainer>
            );
        }
    } else {
        return <ASpinner />;
    }
};

const TrainerTagsContainer = styled.View`
    align-items: flex-start;
    flex-direction: row;
    margin-top: 10px;
`;

const TrainerNumsContainer = styled.View`
    margin-top: 20px;
`;

export const Trainer = ({navigation, route}) => {
    const subjectTagId = route.params.subjectTagId;
    const examTagId = route.params.examTagId;
    const [examTag, setExamTag] = useState();
    const [subject, setSubject] = useState();
    const [stats, setStats] = useState();

    const examTree = useSelector(store => store.examTree.tree);

    const loadTrainerStats = () => {
        getExamSubjectUtr(examTagId, subjectTagId).then(stats =>
            setStats(stats),
        );
    };

    const selectExamTagAndNums = () => {
        for (let examTag of examTree.exams) {
            if (examTag.exam_tag_id === examTagId) {
                setExamTag(examTag);
                // отобрать нужный предмет среди полученного
                for (let subject of examTag.subjects) {
                    if (subjectTagId === subject.subject_tag_id) {
                        setSubject(subject);
                    }
                }
            }
        }
    };

    useEffect(() => {
        loadTrainerStats();
        selectExamTagAndNums();
    }, []);

    return (
        <ViewContainer>
            <ScrollContainer showsVerticalScrollIndicator={false}>
                <TrainerTagsContainer>
                    {examTag && (
                        <ABadge
                            title={examTag.title}
                            variant={'pink'}
                            disabled={true}
                        />
                    )}
                    {subject && (
                        <ABadge
                            title={subject.title}
                            variant={'pink'}
                            disabled={true}
                        />
                    )}
                </TrainerTagsContainer>
                <TrainerStats
                    stats={stats}
                    examTitle={examTag ? examTag.title : false}
                />
                <TrainerNumsContainer>
                    {subject &&
                        subject.nums &&
                        subject.nums.map(num => (
                            <NumComponent
                                key={num.num}
                                num={num}
                                navigation={navigation}
                                subjectTagId={subjectTagId}
                                examTagId={examTagId}
                            />
                        ))}
                </TrainerNumsContainer>
            </ScrollContainer>
        </ViewContainer>
    );
};

export default Trainer;
