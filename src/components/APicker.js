import RNPickerSelect from 'react-native-picker-select';
import styled, {useTheme} from 'styled-components/native';
import React from 'react';
import {InputTitle} from './AInput';

const FieldTitle = styled(InputTitle)`
    margin-top: 15px;
`;

const APicker = props => {
    const theme = useTheme();
    return (
        <>
            {props.title ? <FieldTitle>{props.title}</FieldTitle> : null}
            <RNPickerSelect
                {...props}
                useNativeAndroidPickerStyle={false}
                style={{
                    inputIOS: {
                        fontSize: theme.const.Fonts.sizesInt.input,
                        paddingVertical: 12,
                        paddingHorizontal: 10,
                        borderWidth: 1,
                        borderColor: theme.const.Colors.darkGrayAccent,
                        borderRadius: 5,
                        color: theme.colors.textColor,
                    },
                    inputAndroid: {
                        fontSize: theme.const.Fonts.sizesInt.input,
                        paddingHorizontal: 10,
                        paddingVertical: 8,
                        borderWidth: 1,
                        borderColor: theme.const.Colors.darkGrayAccent,
                        borderRadius: 5,
                        color: theme.colors.textColor,
                    },
                }}
            />
        </>
    );
};

export default APicker;
