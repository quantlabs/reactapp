import {createSlice} from '@reduxjs/toolkit';

export const subjectsSlice = createSlice({
    name: 'subjects',
    initialState: {
        all: [],
    },
    reducers: {
        setSubjects: (state, action) => {
            state.all = action.payload;
        },
        setConceptsForSubject: (state, action) => {
            state.all = state.all.map(subjectTag =>
                action.payload.subjectTagId === subjectTag.id
                    ? // transform the one with a matching id
                      {...subjectTag, concepts: action.payload.concepts}
                    : // otherwise return original object
                      subjectTag,
            );
        },
        updateConcept: (state, action) => {
            state.all = state.all.map(subjectTag =>
                action.payload.subject.id === subjectTag.id
                    ? {
                          ...subjectTag,
                          concepts: subjectTag.concepts.map(conceptTag =>
                              action.payload.id === conceptTag.id
                                  ? action.payload
                                  : conceptTag,
                          ),
                      }
                    : subjectTag,
            );
        },
        setStatsForSubject: (state, action) => {
            state.all = state.all.map(subjectTag =>
                action.payload.subjectTagId === subjectTag.id
                    ? {...subjectTag, stats: action.payload.stats}
                    : subjectTag,
            );
        },
    },
});

// Action creators are generated for each case reducer function
export const {
    setSubjects,
    setConceptsForSubject,
    setStatsForSubject,
    updateConcept,
} = subjectsSlice.actions;

export default subjectsSlice.reducer;
