// React
import React from 'react';
import {StatusBar} from 'react-native';
import {useTheme} from 'styled-components';

const AStatusBar = () => {
    const theme = useTheme();

    return (
        <StatusBar
            backgroundColor={theme.colors.bgColor}
            barStyle={theme.isDark ? 'light-content' : 'dark-content'}
        />
    );
};

export default AStatusBar;
