// React
import React from 'react';

// Styling
import styled from 'styled-components/native';

import {TextSmall} from './Typography';

const Container = styled.View`
    margin-top: 30px;
    border-top-width: 1px;
    border-top-color: ${props => props.theme.colors.textColor};
`;

// подобрано под шрифт 18 px
const TitleText = styled(TextSmall)`
    top: -13px;
    margin-right: ${props =>
        props.theme.device.windowWidth -
        40 -
        10 -
        props.title.length * (17 / 2)}px;
    margin-bottom: -5px;
    background-color: ${props => props.theme.colors.bgColor};
`;

// component to show text with horizontal gray line
export default function ATextWithLine({title}) {
    return (
        <Container>
            <TitleText title={title}>{title}</TitleText>
        </Container>
    );
}
