import styled from 'styled-components/native';

export const RowWrapper = styled.View`
    flex-direction: row;
    justify-content: space-between;
`;

export const ColWrapper = styled.View`
    flex: 0.48;
`;

export const ColDelimeter = styled.View`
    flex: 0.04;
`;
