import {useColorScheme} from 'react-native';

import {createSlice} from '@reduxjs/toolkit';

import {uuidv4} from '../../utils/uuid';

export const settingsSlice = createSlice({
    name: 'settings',
    initialState: {
        appId: null,
        darkMode: false,
        finishOnBoarding: false,
    },
    reducers: {
        setAppId: state => {
            state.appId = uuidv4();
        },
        setDarkMode: (state, action) => {
            state.darkMode = action.payload;
        },
        setFinishOnBoarding: state => {
            state.finishOnBoarding = true;
        },
    },
});

// Action creators are generated for each case reducer function
export const {setAppId, setDarkMode, setFinishOnBoarding} =
    settingsSlice.actions;

export default settingsSlice.reducer;
