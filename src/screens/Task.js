// React
import React, {useEffect, useState} from 'react';
import {
    Alert,
    KeyboardAvoidingView,
    Platform,
    TouchableOpacity,
    View,
} from 'react-native';

// Navigation
import {useNavigation} from '@react-navigation/native';
import {useHeaderHeight} from '@react-navigation/elements';

// Backend
import {questionTypesArray} from '../features/questions/questionTypes';
import {BASE_URL} from '../backend/http';
import {finishUtr, getUtr} from '../features/tasks/tasksApi';
import {
    getQuestion,
    postQuestionAnswer,
} from '../features/questions/questionsApi';

// Styling
import styled from 'styled-components/native';

// Components
import {
    ScrollContainer,
    ViewContainer,
    ViewContainerWithoutPadding,
} from '../components/Containers';
import {Text, TextSmall, TextSmallMuted} from '../components/Typography';
import ASpinner from '../components/ASpinner';
import ATextWithLine from '../components/ATextWithLine';
import ACard from '../components/ACard';
import AInput from '../components/AInput';
import AButton from '../components/AButton';
import AMathMarkdown from '../components/AMathMarkdown';

/**
 * OPTIONS
 */

const OptionCardContainer = styled.TouchableOpacity`
    background-color: ${props => {
        if (props.verified) {
            if (props.isTrue) {
                return props.theme.const.Colors.greenMessage50;
            } else {
                return props.theme.const.Colors.redMessage50;
            }
        } else {
            if (props.selected) {
                return props.theme.const.Colors.blueAccent50;
            } else {
                return props.theme.colors.bgColor;
            }
        }
    }};
    margin-bottom: 8px;
    overflow: hidden;
    flex-direction: row;
    align-items: center;
    border-radius: 5px;
    padding: 10px;
`;

const OptionDetector = styled.View`
    background-color: ${props => {
        if (props.selected) {
            return props.theme.const.Colors.white;
        } else {
            return props.theme.const.Colors.blueAccent;
        }
    }};
    opacity: ${props => {
        if (props.verified && !props.selected) {
            return 0;
        } else {
            return 1;
        }
    }};
    width: 32px;
    height: 32px;
    padding: 5px;
    margin-right: 10px;
    border-radius: ${props => (props.variant === 'radio' ? 16 : 5)}px;
`;

const OptionDetectorInner = styled.View`
    background-color: ${props => {
        if (props.selected) {
            if (props.verified) {
                if (props.isTrue) {
                    return props.theme.const.Colors.greenMessage;
                } else {
                    return props.theme.const.Colors.redMessage;
                }
            } else {
                return props.theme.const.Colors.blueAccent;
            }
        } else {
            return props.theme.const.Colors.white;
        }
    }};
    width: 22px;
    height: 22px;
    border-radius: ${props => (props.variant === 'radio' ? 12 : 3)}px;
`;

const OptionImageContainer = styled.View`
    background-color: 'rgba(255,255,255,1)';
    flex: 1;
`;

const OptionImage = styled.Image`
    width: 100%;
    height: 200px;
    border-radius: 5px;
    overflow: hidden;
`;

const OptionInner = styled.View`
    flex: 1;
    overflow: hidden;
    justify-content: center;
`;

const OptionCard = ({
    option,
    variant = 'radio',
    selected = false,
    verified = false,
    isTrue,
    onClick,
}) => {
    return (
        <OptionCardContainer
            selected={selected}
            verified={verified}
            disabled={verified}
            isTrue={isTrue}
            onPress={() => onClick(option.id)}>
            <OptionDetector
                variant={variant}
                selected={selected}
                verified={verified}>
                <OptionDetectorInner
                    selected={selected}
                    variant={variant}
                    isTrue={isTrue}
                    verified={verified}
                />
            </OptionDetector>
            <OptionInner>
                {option.option_text ? (
                    <AMathMarkdown>{option.option_text}</AMathMarkdown>
                ) : null}
                {option.option_image ? (
                    <OptionImageContainer>
                        <OptionImage
                            resizeMode={'contain'}
                            source={{
                                uri: option.option_image.replace(
                                    '/media',
                                    BASE_URL + '/media',
                                ),
                            }}
                        />
                    </OptionImageContainer>
                ) : null}
            </OptionInner>
        </OptionCardContainer>
    );
};

/**
 * QUESTION
 */

const QuestionExplanations = ({question}) => {
    return (
        <>
            {question.right_answer ? (
                <>
                    <ATextWithLine title={'Правильный ответ'} />
                    <Text>{JSON.stringify(question.right_answer)}</Text>
                </>
            ) : null}
            {question.nodes && question.nodes.length > 0 ? (
                <>
                    <ATextWithLine title={'Знания и навыки'} />
                    {question.nodes.map(node => (
                        <ACard
                            variant={'secondary'}
                            key={node.id}
                            disabled={true}
                            title={node.title}
                        />
                    ))}
                </>
            ) : null}
            {question.explanation_text ? (
                <>
                    <ATextWithLine title={'Комментарий'} />
                    <AMathMarkdown>{question.explanation_text}</AMathMarkdown>
                </>
            ) : null}
        </>
    );
};

const QuestionTextContainer = styled(View)`
    margin-top: 0px;
`;

const QuestionContainer = styled(ViewContainer)`
    padding-bottom: 30px;
`;

const AnswerPanel = styled(View)`
    padding-horizontal: 15px;
    padding-bottom: 15px;
    border-top-width: 1px;
    border-top-color: ${props =>
        props.theme.isDark
            ? props.theme.const.Colors.darkGrayAccent
            : props.theme.const.Colors.lightGrayAccent};
`;

const OptionsContainer = styled.View`
    margin-top: 15px;
`;

const QuestionImageContainer = styled.View`
    background-color: 'rgba(255,255,255,1)';
    flex: 1;
    border-radius: 5px;
    overflow: hidden;
`;

const QuestionImage = styled.Image`
    width: 100%;
    height: 250px;
    border-radius: 5px;
    overflow: hidden;
`;

const Question = ({question, utrId, goToNextQuestion, optionsData}) => {
    const [status, setStatus] = useState(0); // 0 - start, 1 - fetching, 2 - server checks answer
    const [selectedOptions, setSelectedOptions] = useState([]);
    const [answer, setAnswer] = useState('');
    const [questionResult, setQuestionResult] = useState();
    const navigation = useNavigation();

    const questionType = questionTypesArray.find(
        el => question.question_type === el.long,
    );

    const onOptionClick = selectedOptionId => {
        if (question.is_multiple) {
            const exists = selectedOptions.includes(selectedOptionId);
            if (exists) {
                const newArray = selectedOptions.filter(op => {
                    return op !== selectedOptionId;
                });
                setSelectedOptions(newArray);
            } else {
                const newArray = [...selectedOptions, selectedOptionId];
                setSelectedOptions(newArray);
            }
        } else {
            setSelectedOptions([selectedOptionId]);
        }
    };

    const onAnswerChange = e => {
        setAnswer(e);
    };

    const sendAnswer = () => {
        let answerEdited;
        if (questionType.short === 'choice') {
            answerEdited = selectedOptions;
        } else if (questionType.short === 'float') {
            let _answer = answer.replace(',', '.');
            if (isNaN(+_answer)) {
                Alert.alert('Введите число');
                return false;
            } else {
                answerEdited = _answer;
            }
        } else {
            answerEdited = answer;
        }

        setStatus(1);
        postQuestionAnswer(utrId, question.id, answerEdited).then(data => {
            setQuestionResult({
                ...data.result,
                nextQuestionId:
                    data.next_question_pk > 0 ? data.next_question_pk : false,
                isTrue: data.result.max_score === data.result.score,
            });
            setStatus(2);
        });
    };

    return (
        <View style={{flex: 1}}>
            <ScrollContainer showsVerticalScrollIndicator={false}>
                <QuestionContainer>
                    <TextSmallMuted>
                        Задание #{question.id} {questionType.short}
                    </TextSmallMuted>
                    <QuestionTextContainer>
                        <AMathMarkdown>{question.question_text}</AMathMarkdown>
                    </QuestionTextContainer>
                    {question.image ? (
                        <QuestionImageContainer>
                            <QuestionImage
                                resizeMode={'contain'}
                                source={{
                                    uri: question.image.replace(
                                        '/media',
                                        BASE_URL + '/media',
                                    ),
                                }}
                            />
                        </QuestionImageContainer>
                    ) : null}
                    {questionType.short === 'choice' && optionsData && (
                        <OptionsContainer>
                            <TextSmallMuted>
                                {question.is_multiple
                                    ? 'Выбери все верные ответы'
                                    : 'Выбери верный ответ'}
                            </TextSmallMuted>
                            {optionsData.map(option => (
                                <OptionCard
                                    key={option.id}
                                    option={option}
                                    selected={selectedOptions.includes(
                                        option.id,
                                    )}
                                    verified={status > 0}
                                    isTrue={option.is_true}
                                    onClick={onOptionClick}
                                    variant={
                                        question.is_multiple
                                            ? 'checkbox'
                                            : 'radio'
                                    }
                                />
                            ))}
                        </OptionsContainer>
                    )}
                    {status === 2 ? (
                        <QuestionExplanations question={question} />
                    ) : null}
                </QuestionContainer>
            </ScrollContainer>
            <AnswerPanel>
                {questionType.short !== 'choice' && (
                    <AInput
                        value={answer}
                        disabled={status > 0}
                        onChangeText={onAnswerChange}
                        hasError={status === 2 && !questionResult.isTrue}
                        hasSuccess={status === 2 && questionResult.isTrue}
                        placeholder={'Введи ответ'}
                    />
                )}
                {status === 0 && (
                    <AButton
                        title={'Проверить ответ'}
                        disabled={selectedOptions.length === 0 && answer === ''}
                        onPress={sendAnswer}
                    />
                )}
                {status === 1 && (
                    <AButton title={'Загрузка...'} disabled={true} />
                )}
                {status === 2 && questionResult.nextQuestionId && (
                    <AButton
                        title={'Дальше'}
                        onPress={() => {
                            setAnswer('');
                            setStatus(0);
                            setSelectedOptions([]);
                            setQuestionResult(null);
                            goToNextQuestion(questionResult.nextQuestionId);
                        }}
                    />
                )}
                {status === 2 && !questionResult.nextQuestionId && (
                    <AButton
                        title={'Закончить тестирование'}
                        onPress={() => {
                            finishUtr(utrId).then(res => {
                                setAnswer('');
                                setStatus(0);
                                setSelectedOptions([]);
                                setQuestionResult(null);
                                navigation.goBack();
                            });
                        }}
                    />
                )}
            </AnswerPanel>
        </View>
    );
};

/**
 * TASK
 */

const TaskContainer = styled(KeyboardAvoidingView)`
    flex: 1;
`;

const Task = ({route}) => {
    const utrId = route.params.utrId;
    const [utrData, setUtrData] = useState();

    const [currentQuestionId, setCurrentQuestionId] = useState();
    const [question, setQuestion] = useState();
    const [optionsData, setOptionsData] = useState();

    const headerHeight = useHeaderHeight();

    const goToNextQuestion = nextQuestionId => {
        setOptionsData(null);
        setCurrentQuestionId(nextQuestionId);
    };

    useEffect(() => {
        getUtr(utrId).then(utr => {
            setUtrData(utr);
            setCurrentQuestionId(utr.task.questions[utr.current_question_idx]);
        });
    }, [utrId]);

    useEffect(() => {
        if (currentQuestionId) {
            getQuestion(currentQuestionId).then(data => {
                if (data.options_data) {
                    setOptionsData(
                        data.options_data.sort(() => 0.5 - Math.random()),
                    );
                }
                setQuestion(data);
            });
        }
    }, [currentQuestionId]);

    return (
        <TaskContainer
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            keyboardVerticalOffset={
                Platform.OS === 'ios' ? headerHeight + 47 : headerHeight
            }>
            <ViewContainerWithoutPadding>
                {utrData && question ? (
                    <Question
                        question={question}
                        optionsData={optionsData}
                        utrId={utrId}
                        goToNextQuestion={goToNextQuestion}
                    />
                ) : (
                    <ASpinner />
                )}
            </ViewContainerWithoutPadding>
        </TaskContainer>
    );
};

export default Task;
