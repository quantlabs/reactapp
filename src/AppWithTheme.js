// React
import React from 'react';
import {Dimensions} from 'react-native';

// Redux
import {useSelector} from 'react-redux';

// Theme
import {ThemeProvider} from 'styled-components';

// Theme constants
import {Colors, Fonts, Views} from './style/constants';

// Base theme
const baseTheme = {
    isDark: false,
    device: {
        windowWidth: Dimensions.get('window').width,
        containerWidth: Dimensions.get('window').width - 2 * 15,
    },
    colors: {
        bgColor: Colors.white,
        textColor: Colors.black,
    },
    const: {
        Colors,
        Fonts,
        Views,
    },
};

const lightTheme = baseTheme;

const darkTheme = {
    ...baseTheme,
    colors: {
        bgColor: Colors.black,
        textColor: Colors.white,
    },
    isDark: true,
};

// Provider for add useState for nested components
export default function AppWithTheme(props) {
    const darkMode = useSelector(store => store.settings.darkMode);

    return (
        <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
            {props.children}
        </ThemeProvider>
    );
}
