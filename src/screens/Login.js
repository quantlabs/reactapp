// React
import React from 'react';
import {View} from 'react-native';

// Styling
import styled from 'styled-components/native';

// Our Components
import AButton from '../components/AButton';
import ALogo from '../components/ALogo';
import {ViewContainerWithSpace} from '../components/Containers';
import {TextH2Bold, Text} from '../components/Typography';

const LogoContainer = styled.View`
    padding-top: 100px;
`;

const Descriptor = styled(Text)`
    margin-top: 20px;
    margin-bottom: 20px;
`;

export default function Login({navigation}) {
    const VK_APP_ID = 7687895;

    const vkLogin = async () => {
        console.log('Start VK browser authorization');
    };

    // TODO: VK native app authorization after expo eject
    const vkLoginNew = async () => {
        console.log('Start native VK app auth');
    };

    // TODO: fix apple authentication
    const appleLogin = async () => {
        console.log('Apple login');
    };

    const emailLogin = async () => {
        navigation.navigate('Email Login');
    };

    return (
        <ViewContainerWithSpace>
            <LogoContainer>
                <ALogo width={200} />
            </LogoContainer>
            <View>
                <TextH2Bold>Привет!</TextH2Bold>
                <Descriptor>
                    Погнали исправлять оценки и сдавать экзамен на отлично!
                </Descriptor>
                <AButton title="Войти через VK" onPress={vkLogin} />
                <AButton title="Войти через email" onPress={emailLogin} />
            </View>
        </ViewContainerWithSpace>
    );
}
