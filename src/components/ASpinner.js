// React
import React from 'react';
import {ActivityIndicator} from 'react-native';

// Styling
import styled from 'styled-components/native';

const ActivityIndicatorContainer = styled.View`
    padding: 10px;
`;

const ASpinner = props => {
    const size = props.size || 'large';
    return (
        <ActivityIndicatorContainer>
            <ActivityIndicator size={size} />
        </ActivityIndicatorContainer>
    );
};

export default ASpinner;
