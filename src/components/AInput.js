// React
import React from 'react';

// Styling
import styled, {useTheme} from 'styled-components/native';

const InputContainer = styled.View`
    margin-top: 15px;
`;

export const InputTitle = styled.Text`
    color: ${props => props.theme.const.Colors.darkGrayAccent};
    font-family: ${props => props.theme.const.Fonts.family.main};
    font-size: ${props => props.theme.const.Fonts.sizes.small};
`;

const InputErrors = styled.Text`
    color: ${props => props.theme.const.Colors.redMessage};
    font-family: ${props => props.theme.const.Fonts.family.main};
    font-size: ${props => props.theme.const.Fonts.sizes.small};
`;

const InputField = styled.TextInput`
    height: 48px;
    border-width: 1px;
    border-radius: 5px;
    font-size: ${props => props.theme.const.Fonts.sizes.input};
    font-family: ${props => props.theme.const.Fonts.family.main};
    border-color: ${props => {
        if (props.error) {
            return props.theme.const.Colors.redMessage;
        } else if (props.hasError) {
            return props.theme.const.Colors.redMessage;
        } else if (props.hasSuccess) {
            return props.theme.const.Colors.greenMessage;
        } else {
            return props.theme.const.Colors.darkGrayAccent;
        }
    }};
    padding-horizontal: 10px;
    color: ${props => props.theme.colors.textColor};
    background-color: ${props => props.theme.colors.bgColor};
`;

// Кроме обычных опций TextInput принимает:
//  1. title={string}
//  2. error={string}
//  3. customStyle={Object}
export const AInput = ({title, error, ...props}) => {
    const theme = useTheme();

    return (
        <InputContainer>
            {title && <InputTitle>{title}</InputTitle>}
            <InputField
                {...props}
                placeholderTextColor={theme.const.Colors.darkGrayAccent}
            />
            {error && <InputErrors>{error}</InputErrors>}
        </InputContainer>
    );
};

export default AInput;
