// React
import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

// Styling
import styled from 'styled-components/native';

const BadgeText = styled.Text`
    color: ${props => {
        if (props.variant === 'pink') {
            return props.theme.const.Colors.white;
        } else {
            if (props.theme.isDark) {
                return props.theme.const.Colors.white;
            } else {
                return props.theme.const.Colors.black;
            }
        }
    }};
    font-family: ${props => {
        if (props.variant === 'pink') {
            return props.theme.const.Fonts.family.bold;
        } else {
            return props.theme.const.Fonts.family.main;
        }
    }};
    overflow: hidden;
    font-size: 15px;
    line-height: 20px;
`;

const BadgeContainer = styled.View`
    height: 30px;
    padding-vertical: 5px;
    padding-horizontal: 10px;
    border-radius: 15px;
    text-align: center;
    margin-right: 5px;
    text-align-vertical: center;
    background-color: ${props => {
        if (props.variant === 'pink') {
            return props.theme.const.Colors.pinkAccent;
        } else {
            if (props.theme.isDark) {
                return props.theme.const.Colors.darkGrayAccent;
            } else {
                return props.theme.const.Colors.brightGrayAccent;
            }
        }
    }};
`;

// TODO: add color scheme
const ABadge = ({onPress, title, disabled, variant, ...props}) => {
    return (
        <TouchableOpacity onPress={onPress} disabled={disabled}>
            <BadgeContainer variant={variant} style={props.customStyles}>
                <BadgeText variant={variant}>{title}</BadgeText>
            </BadgeContainer>
        </TouchableOpacity>
    );
};

export default ABadge;
