/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StatusBar, View, Switch, Alert} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {
    setDarkMode,
    setFinishOnBoarding,
} from '../features/settings/settingsSlice';
import styled from 'styled-components/native';
import AButton from '../components/AButton';
import {logout, setToken} from '../features/auth/authSlice';
import {ViewContainer} from '../components/Containers';

const Text = styled.Text`
    color: ${props => props.theme.colors.textColor};
`;

const Example = () => {
    const settings = useSelector(store => store.settings);
    const auth = useSelector(store => store.auth);
    const dispatch = useDispatch();

    return (
        <ViewContainer>
            {/*<Text>Page Content</Text>*/}
            <Text>Auth: {JSON.stringify(auth)}</Text>
            {/*<AButton*/}
            {/*    title={'Throw error to Sentry'}*/}
            {/*    onPress={() => {*/}
            {/*        throw new Error('My first Sentry error!');*/}
            {/*    }}*/}
            {/*/>*/}
            <AButton title={'Выйти'} onPress={() => dispatch(logout())} />
            <AButton
                title={'Дальше'}
                onPress={() => dispatch(setFinishOnBoarding())}
            />
            <View>
                <Text>Темная тема</Text>
                <Switch
                    value={settings.darkMode}
                    onValueChange={value => dispatch(setDarkMode(value))}
                />
            </View>
        </ViewContainer>
    );
};

export default Example;
