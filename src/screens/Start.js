/**
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {View} from 'react-native';
import {setFinishOnBoarding} from '../features/settings/settingsSlice';
import styled from 'styled-components/native';
import AButton from '../components/AButton';
import {ViewContainer} from '../components/Containers';
import ALogo from '../components/ALogo';
import {Text, TextH2Bold, TextSmall} from '../components/Typography';
import {useDispatch, useSelector} from 'react-redux';

const Container = styled(ViewContainer)`
    justify-content: flex-end;
`;

const Descriptor = styled(Text)`
    margin-top: 20px;
    margin-bottom: 50px;
`;

const Start = ({navigation}) => {
    // 0 - start
    // 1 - choice between login and registration
    const [stage, setStage] = useState(0);

    const dispatch = useDispatch();

    const toLogin = () => {
        navigation.navigate('Email Login');
    };

    const toRegistration = () => {
        navigation.navigate('Registration');
    };

    // https://fingers.by/blog/mobile-app-log-in
    return (
        <Container>
            <ALogo width={200} />
            <Descriptor>
                Приложение для диагностики твоих знаний по школьным предметам
            </Descriptor>
            <AButton title="Вход" variant={'secondary'} onPress={toLogin} />
            <AButton
                title="Регистрация"
                onPress={toRegistration}
            />
        </Container>
    );
};

export default Start;
