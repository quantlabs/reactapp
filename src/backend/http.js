import axios from 'axios';
import {store} from '../redux';

// Errors handling
// https://www.freecodecamp.org/news/how-to-use-axios-with-react/

export const BASE_URL = 'https://stagging.adaptaki.ru';
export const API_URL = `${BASE_URL}/api/`;
export const MEDIA_URL = `${BASE_URL}/media/`;

const fetchClient = (contentType = 'json') => {
    const axiosConfig = {
        baseURL: API_URL,
        headers:
            contentType === 'json'
                ? {
                      'content-type': 'application/json',
                  }
                : {
                      'content-type': 'multipart/form-data',
                      // boundary: 'another cool boundary',
                  },
    };

    const httpClient = axios.create(axiosConfig);

    httpClient.interceptors.request.use(function (config) {
        const token = store.getState().auth.token;
        if (token) {
            config.headers.Authorization = `Token ${token}`;
        }
        return config;
    });
    return httpClient;
};

export const handleError = (error, description) => {
    // Error
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(
            description,
            error.response.data,
            error.response.status,
            error.response.headers,
        );
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the
        // browser and an instance of
        // http.ClientRequest in node.js
        console.log(description, error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log(description, 'Error', error.message);
    }
};

export const fetchWithDataClient = fetchClient('file');

export default fetchClient('json');
