// React
import React from 'react';
import {View} from 'react-native';

// Images
import {LOGO_GRAY, LOGO_WHITE} from '../assets/images';
import styled from 'styled-components/native';
import {useTheme} from 'styled-components';

const Logo = styled.Image`
    width: ${props => props.width}px;
    height: ${props => props.width * 0.193}px;
`;

// TODO: add dark theme support
const ALogo = ({width = 100}) => {
    const theme = useTheme();

    return (
        <View>
            <Logo
                source={theme.isDark ? LOGO_WHITE : LOGO_GRAY}
                width={width}
            />
        </View>
    );
};

export default ALogo;
