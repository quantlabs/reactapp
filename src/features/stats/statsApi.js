import fetchClient, {handleError} from '../../backend/http';
import {store} from '../../redux';
import {setStatsForSubject} from '../subjects/subjectsSlice';

export const getConceptsStats = async subjectTagId => {
    let result = await fetchClient
        .get(`stat/concepts/${subjectTagId ? `?subject=${subjectTagId}` : ''}`)
        .then(res => {
            if (subjectTagId) {
                store.dispatch(
                    setStatsForSubject({subjectTagId, stats: res.data}),
                );
            }
        })
        .catch(error => {
            handleError(error, 'get concepts stats error');
        });
    return result;
};

export const getConceptStats = async conceptTagId => {
    let result = await fetchClient
        .get(`stat/concept/${conceptTagId}/`)
        .then(res => {
            return res.data;
        })
        .catch(error => {
            handleError(error, 'get concept stats error');
        });
    return result;
};

export const getExamSubjectNumStats = async (examTagId, subjectTagId, num) => {
    let result = await fetchClient
        .get(`stat/exam/?exam=${examTagId}&subject=${subjectTagId}&num=${num}`)
        .then(res => {
            return res.data;
        })
        .catch(error => {
            handleError(error, 'get ExamSubjectNum stats error');
        });
    return result;
};

export const getExamThemeStats = async themeId => {
    let result = await fetchClient
        .get(`stat/exam/?theme=${themeId}`)
        .then(res => {
            return res.data;
        })
        .catch(error => {
            handleError(error, 'get ExamTheme stats error');
        });
    return result;
};
