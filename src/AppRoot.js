/**
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';

// Redux
import {useSelector, useDispatch} from 'react-redux';
import {setAppId, setDarkMode} from './features/settings/settingsSlice';

// Features
import {checkAuth, getUser} from './features/auth/authApi';
import {getExamTree} from './features/examTree/examTreeApi';
import {getSubjects} from './features/subjects/subjectsApi';

// Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// Styling
import styled, {useTheme} from 'styled-components/native';

// Components
import AStatusBar from './components/AStatusBar';
import ALogo from './components/ALogo';

// Screens
import Login from './screens/Login';
import EmailLogin from './screens/EmailLogin';
import Dashboard from './screens/Dashboard';
import Splash from './screens/Splash';
import Profile from './screens/Profile';
import Subject from './screens/Subject';
import Concept from './screens/Concept';
import Trainer from './screens/Trainer';
import Task from './screens/Task';
import Example from './screens/Example';
import Start from './screens/Start';
import Hello from './screens/Hello';
import Registration from './screens/Registration';
import {useColorScheme} from 'react-native';

const AppContainer = styled.SafeAreaView`
    flex: 1;
    background-color: ${props => props.theme.colors.bgColor};
`;

const Stack = createNativeStackNavigator();

const AppRoot = () => {
    const dispatch = useDispatch();

    const [signedIn, setSignedIn] = useState();
    const [isFetching, setIsFetching] = useState(true);

    const appId = useSelector(store => store.settings.appId);
    const darkMode = useSelector(store => store.settings.darkMode);
    const finishOnBoarding = useSelector(
        store => store.settings.finishOnBoarding,
    );
    const token = useSelector(store => store.auth.token);

    const theme = useTheme();
    const scheme = useColorScheme();

    const navigationContainerTheme = {
        dark: true,
        colors: {
            primary: theme.colors.bgColor,
            background: theme.colors.bgColor,
            card: theme.colors.bgColor,
            text: theme.colors.textColor,
            border: theme.colors.bgColor,
            notification: theme.colors.bgColor,
        },
    };

    const stackNavigationScreenOptions = {
        headerStyle: {
            backgroundColor: theme.colors.bgColor,
            shadowColor: 'transparent',
            shadowRadius: 0,
            shadowOffset: {
                height: 0,
            },
        },
        headerTransparent: false,
        headerTintColor: theme.colors.textColor,
        headerTitleStyle: {
            fontWeight: 'normal',
            fontFamily: theme.const.Fonts.family.main,
        },
        headerBackTitle: '',
        headerShadowVisible: false,
    };

    // Set unique app identifier on app starts if empty
    useEffect(() => {
        if (!appId) {
            dispatch(setAppId());
        }

        if (!darkMode) {
            dispatch(setDarkMode(scheme === 'dark'));
        }
    }, []);

    // Get exam tree and subjects on init or token change
    useEffect(() => {
        if (finishOnBoarding) {
            setIsFetching(true);
            if (token) {
                checkAuth().then(isAuth => {
                    if (isAuth) {
                        Promise.all([
                            getUser(),
                            getExamTree(),
                            getSubjects(),
                        ]).then(() => {
                            setIsFetching(false);
                        });
                        setSignedIn('signedIn');
                    } else {
                        setSignedIn('noAccess');
                        setIsFetching(false);
                    }
                });
            } else {
                setSignedIn('noAccess');
                setIsFetching(false);
            }
        } else {
            setSignedIn('onBoarding');
        }
    }, [token, finishOnBoarding]);

    const screens = () => {
        if (signedIn === 'signedIn' && !isFetching) {
            return (
                <Stack.Group
                    screenOptions={{
                        headerTitle: () => <ALogo size={100} />,
                    }}>
                    <Stack.Screen name="Dashboard" component={Dashboard} />
                    <Stack.Screen
                        name="Profile"
                        component={Profile}
                        options={{title: 'Профиль'}}
                    />
                    <Stack.Screen
                        name="Subject"
                        component={Subject}
                        options={{title: 'Предмет'}}
                    />
                    <Stack.Screen
                        name="Concept"
                        component={Concept}
                        options={{title: 'Концепт'}}
                    />
                    <Stack.Screen
                        name="Trainer"
                        component={Trainer}
                        options={{title: 'Тренажер'}}
                    />
                    <Stack.Screen
                        name="Task"
                        component={Task}
                        options={{title: 'Таск'}}
                    />
                </Stack.Group>
            );
        } else if (signedIn === 'noAccess') {
            return (
                <>
                    <Stack.Screen
                        name="Start"
                        component={Start}
                        options={{title: 'Старт', headerShown: false}}
                    />
                    <Stack.Screen
                        name="Email Login"
                        component={EmailLogin}
                        options={{
                            title: 'Вход через Email',
                        }}
                    />
                    <Stack.Screen
                        name="Registration"
                        component={Registration}
                        options={{
                            title: 'Регистрация',
                        }}
                    />
                </>
            );
        } else if (signedIn === 'onBoarding') {
            return (
                <>
                    <Stack.Screen
                        name="Example"
                        component={Hello}
                        options={{title: 'Старт', headerShown: false}}
                    />
                </>
            );
        } else {
            return (
                <Stack.Screen
                    name="Splash"
                    component={Splash}
                    options={{
                        title: 'Splash',
                        headerShown: false,
                    }}
                />
            );
        }
    };

    return (
        <AppContainer>
            <AStatusBar />
            <NavigationContainer theme={navigationContainerTheme}>
                <Stack.Navigator screenOptions={stackNavigationScreenOptions}>
                    {screens()}
                </Stack.Navigator>
            </NavigationContainer>
        </AppContainer>
    );
};

export default AppRoot;
