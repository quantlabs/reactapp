import styled from 'styled-components/native';

export const ViewContainerWithoutPadding = styled.SafeAreaView`
    flex: 1;
    background-color: ${props => props.theme.colors.bgColor};
`;

export const ViewContainer = styled.View`
    flex: 1;
    background-color: ${props => props.theme.colors.bgColor};
    padding-horizontal: ${props => props.theme.const.Views.paddingHorizontal};
`;

export const ScrollContainer = styled.ScrollView`
    padding-bottom: 20px;
`;

export const ViewContainerWithSpace = styled(ViewContainer)`
    justify-content: space-between;
`;
