import fetchClient, {handleError} from '../../backend/http';
import {store} from '../../redux';
import {
    setConceptsForSubject,
    setStatsForSubject,
    setSubjects,
    updateConcept,
} from './subjectsSlice';

export const getConceptsBySubjectTagId = async subjectTagId => {
    await fetchClient
        .get(`tags/concept/active/?subject=${subjectTagId}`)
        .then(res => {
            store.dispatch(
                setConceptsForSubject({subjectTagId, concepts: res.data}),
            );
        })
        .catch(error => {
            handleError(error, 'fetch concepts for subject error');
        });
};

export const getConceptById = async conceptTagId => {
    let result = await fetchClient
        .get(`tags/concept/${conceptTagId}/`)
        .then(res => {
            store.dispatch(updateConcept(res.data));
            return res.data;
        })
        .catch(error => {
            handleError(error, 'fetch concept error');
        });
    return result;
};

export const getSubjects = async () => {
    let result = await fetchClient
        .get('tags/subjects/')
        .then(res => {
            let subjectTags = res.data.map(sTag => {
                return {
                    id: sTag.id,
                    title: sTag.subject.title,
                    subject: sTag.subject,
                };
            });
            store.dispatch(setSubjects(subjectTags));
        })
        .catch(error => {
            handleError(error, 'get subjects error');
        });
    return result;
};
