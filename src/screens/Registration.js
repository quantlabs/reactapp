// React
import React, {useState} from 'react';

// Redux
import {useDispatch, useSelector} from 'react-redux';

// Styling
import styled from 'styled-components/native';

// Components
import AButton from '../components/AButton';
import AInput from '../components/AInput';
import {ViewContainer} from '../components/Containers';
import {Text} from '../components/Typography';
import {View} from 'react-native';
import {Formik} from 'formik';
import APicker from '../components/APicker';

const validate = values => {
    const errors = {};

    if (values.name === '') {
        errors.name = 'Введите имя';
    }

    if (!values.username) {
        errors.username = 'Введите email';
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.username)
    ) {
        errors.username = 'Некорректный адрес';
    }

    if (!values.password) {
        errors.password = 'Введите пароль';
    } else if (!values.password1) {
        errors.password1 = 'Подтвердите пароль';
    } else if (values.password !== values.password1) {
        errors.password = 'Пароли не совпадают';
        errors.password1 = 'Пароли не совпадают';
    }

    console.log(errors);
    return errors;
};

// TODO: fix registration link
export function Registration() {
    // 0 - start
    // 1 - choice between login and registration
    const [stage, setStage] = useState(0);

    const register = (values, setSubmitting) => {};

    return (
        <ViewContainer>
            <Text>Регистрация</Text>
            <Formik
                initialValues={{
                    name: '',
                    level: null,
                    username: 'root@root.root',
                    password: 'root',
                    password1: '',
                }}
                validate={values => validate(values)}
                onSubmit={(values, {setSubmitting}) => {
                    console.log(values);
                    setSubmitting(false);
                }}>
                {({
                    handleChange,
                    setFieldValue,
                    handleBlur,
                    handleSubmit,
                    values,
                    isSubmitting,
                    errors,
                    touched,
                }) => (
                    <View>
                        <AInput
                            title={'Имя'}
                            value={values.name}
                            onChangeText={handleChange('name')}
                            error={errors.name && touched.name && errors.name}
                        />
                        <APicker
                            title={'Уровень'}
                            onValueChange={value =>
                                setFieldValue('level', value)
                            }
                            value={values.level}
                            placeholder={{
                                label: 'Не выбран...',
                                value: null,
                            }}
                            items={[
                                {label: 'Отличник', value: '5'},
                                {label: 'Ударник', value: '4'},
                                {label: 'Слабо разбираюсь', value: '3'},
                                {label: 'Ничего не понимаю', value: '2'},
                            ]}
                        />
                        <AInput
                            title={'Email'}
                            onChangeText={handleChange('username')}
                            onBlur={handleBlur('username')}
                            value={values.username}
                            autoCorrect={false}
                            autoCapitalize={'none'}
                            returnKeyType={'next'}
                            textContentType={'emailAddress'}
                            autoCompleteType={'email'}
                            keyboardType={'email-address'}
                            error={
                                errors.username &&
                                touched.username &&
                                errors.username
                            }
                        />
                        <AInput
                            title={'Пароль'}
                            onChangeText={handleChange('password')}
                            onBlur={handleBlur('password')}
                            value={values.password}
                            autoCorrect={false}
                            autoCapitalize={'none'}
                            clearTextOnFocus
                            enablesReturnKeyAutomatically
                            textContentType={'password'}
                            autoCompleteType={'password'}
                            secureTextEntry={true}
                            error={
                                errors.password &&
                                touched.password &&
                                errors.password
                            }
                        />
                        <AInput
                            title={'Подтвердите пароль'}
                            onChangeText={handleChange('password1')}
                            onBlur={handleBlur('password1')}
                            value={values.password1}
                            autoCorrect={false}
                            autoCapitalize={'none'}
                            clearTextOnFocus
                            enablesReturnKeyAutomatically
                            textContentType={'password'}
                            autoCompleteType={'password'}
                            secureTextEntry={true}
                            error={
                                errors.password1 &&
                                touched.password1 &&
                                errors.password1
                            }
                        />
                        <AButton
                            onPress={handleSubmit}
                            title="Войти"
                            variant={
                                Object.keys(errors).length === 0
                                    ? 'default'
                                    : 'secondary'
                            }
                            disabled={
                                Object.keys(errors).length !== 0 || isSubmitting
                            }
                        />
                    </View>
                )}
            </Formik>
        </ViewContainer>
    );
}

export default Registration;
