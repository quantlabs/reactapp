/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

// React
import React from 'react';
import type {Node} from 'react';

// Sentry
import * as Sentry from '@sentry/react-native';

// Our components
import AppWithStore from './AppWithStore';
import AppRoot from './AppRoot';
import AppWithTheme from './AppWithTheme';

Sentry.init({
    dsn: 'https://b2608fd2c6954528ba1ba7151383e923@o1048572.ingest.sentry.io/6106574',
    enableNative: false,
});

const App: () => Node = () => {
    return (
        <AppWithStore>
            <AppWithTheme>
                <AppRoot />
            </AppWithTheme>
        </AppWithStore>
    );
};

export default Sentry.wrap(App);
