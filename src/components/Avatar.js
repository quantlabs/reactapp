import React from 'react';

import {useSelector} from 'react-redux';

import {MEDIA_URL} from '../backend/http';

import styled from 'styled-components/native';

import {EMPTY_AVATAR} from '../assets/images';

const AvatarImage = styled.Image`
    border-radius: 5px;
    width: ${props => props.size}px;
    height: ${props => props.size}px;
    overflow: hidden;
`;

export const UserAvatar = ({size = 40}) => {
    const user = useSelector(state => state.auth.user);

    return (
        <AvatarImage
            size={size}
            source={
                user.avatar
                    ? {
                          uri: user.avatar.replace('/media', MEDIA_URL),
                      }
                    : EMPTY_AVATAR
            }
        />
    );
};
