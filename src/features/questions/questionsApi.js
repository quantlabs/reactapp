import fetchClient, {handleError} from '../../backend/http';

export const getQuestion = async questionId => {
    let result = await fetchClient
        .get(`questions/${questionId}/?answer=True`)
        .then(res => res.data)
        .catch(error => {
            handleError(error, 'get question error');
        });
    return result;
};

export const postQuestionAnswer = async (utrId, questionId, answer) => {
    let result = await fetchClient
        .post(`utrs/${utrId}/questions/${questionId}/`, {answer: answer})
        .then(res => res.data)
        .catch(error => {
            handleError(error, 'post question answer error');
        });
    return result;
};
