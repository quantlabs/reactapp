/**
 * @format
 * @flow strict-local
 */

import React, {useCallback, useRef, useState} from 'react';
import {setFinishOnBoarding} from '../features/settings/settingsSlice';
import styled, {useTheme} from 'styled-components/native';
import AButton from '../components/AButton';
import {ViewContainer} from '../components/Containers';
import ALogo from '../components/ALogo';
import {Text, TextH2, TextH2Bold, TextSmall} from '../components/Typography';
import {useDispatch, useSelector} from 'react-redux';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {View} from 'react-native';

const Container = styled(ViewContainer)`
    justify-content: flex-end;
`;

const Descriptor = styled(Text)`
    margin-top: 20px;
    margin-bottom: 20px;
    text-align: center;
`;

const TopContainer = styled(View)`
    align-items: center;
`;

const CarouselContainer = styled.View`
    padding-vertical: 10px;
`;

const CarouselItemContainer = styled.View`
    background-color: ${props => props.theme.colors.bgColor};
    border-radius: 5px;
    height: 300px;
    padding: 30px;
    margin-vertical: 20px;
    margin-horizontal: 15px;
    shadow-opacity: 0.2;
    shadow-radius: 10px;
    shadow-color: ${props => props.theme.colors.textColor};
`;

const CarouselTitle = styled(TextH2)`
    margin-bottom: 20px;
`;

const carouselItems = [
    {
        title: 'Решай',
        text: 'На нашей платформе собраны тесты по многим темам математики и русского языка',
    },
    {
        title: 'Получай рекомендации',
        text: 'Наши умные алгоритмы найдут слабые места в твоей подготовке и предложат их исправить',
    },
    {
        title: 'Будь уверен',
        text: 'Решая задания на нашей платформе ты получишь прогноз твоего результата на экзамене',
    },
];

const Hello = ({navigation}) => {
    const dispatch = useDispatch();
    const theme = useTheme();

    const calouselRef = useRef(null);
    const [activeCarouselIndex, setActiveCarouselIndex] = useState(0);

    const toStart = () => {
        dispatch(setFinishOnBoarding());
    };

    const renderCarouselItem = useCallback(
        ({item, index}) => (
            <CarouselItemContainer>
                <CarouselTitle>{item.title}</CarouselTitle>
                <TextSmall>{item.text}</TextSmall>
            </CarouselItemContainer>
        ),
        [],
    );

    // https://fingers.by/blog/mobile-app-log-in
    return (
        <Container>
            <TopContainer>
                <ALogo width={200} />
                <Descriptor>Твой помощник в школьных предметах</Descriptor>
            </TopContainer>
            <CarouselContainer>
                <Carousel
                    layout={'default'}
                    ref={calouselRef}
                    data={carouselItems}
                    sliderWidth={theme.device.containerWidth}
                    itemWidth={theme.device.containerWidth}
                    renderItem={renderCarouselItem}
                    onSnapToItem={(index: number) =>
                        setActiveCarouselIndex(index)
                    }
                    autoplay={true}
                    autoplayDelay={1000}
                    autoplayInterval={5000}
                />
                <Pagination
                    dotsLength={carouselItems.length}
                    activeDotIndex={activeCarouselIndex}
                    containerStyle={{backgroundColor: theme.colors.bgColor}}
                    dotStyle={{
                        width: 10,
                        height: 10,
                        borderRadius: 5,
                        marginHorizontal: 8,
                        backgroundColor: theme.colors.textColor,
                    }}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                    tappableDots={true}
                    carouselRef={calouselRef}
                />
            </CarouselContainer>
            <AButton title="Дальше" onPress={toStart} />
        </Container>
    );
};

export default Hello;
