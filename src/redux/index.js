// Redux
import {configureStore, combineReducers} from '@reduxjs/toolkit';

// Redux-persist
import {
    persistStore,
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Reducers
import authReducer from '../features/auth/authSlice';
import subjectsReducer from '../features/subjects/subjectsSlice';
import examTreeReducer from '../features/examTree/examTreeSlice';
import settingsReducer from '../features/settings/settingsSlice';

// Config for local storage
const persistConfig = {
    key: 'root15',
    storage: AsyncStorage,
};

// https://redux-toolkit.js.org/usage/usage-guide#use-with-redux-persist

let combineReducer = combineReducers({
    auth: authReducer,
    subjects: subjectsReducer,
    examTree: examTreeReducer,
    settings: settingsReducer,
});

const persistedReducer = persistReducer(persistConfig, combineReducer);

export const store = configureStore({
    reducer: persistedReducer,
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [
                    FLUSH,
                    REHYDRATE,
                    PAUSE,
                    PERSIST,
                    PURGE,
                    REGISTER,
                ],
            },
        }),
});

export let persistor = persistStore(store);
