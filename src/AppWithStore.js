// React
import React from 'react';

// Redux
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

// Our store
import {store, persistor} from './redux';

// Persist Gate for save store locally
// Provider for add useState for nested components
export default function AppWithStore(props) {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                {props.children}
            </PersistGate>
        </Provider>
    );
}
