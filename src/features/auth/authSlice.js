import {createSlice} from '@reduxjs/toolkit';

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        token: null,
        user: {},
    },
    reducers: {
        setToken: (state, action) => {
            state.token = action.payload;
        },
        logout: (state, action) => {
            (state.token = null), (state.user = {});
        },
        setUser: (state, action) => {
            state.user = action.payload;
        },
        setPhoto: (state, action) => {
            state.photo = action.payload;
        },
        updateUser: (state, action) => {
            state.user = Object.assign({}, state.user, action.payload);
        },
    },
});

// Action creators are generated for each case reducer function
export const {setToken, logout, setUser, setPhoto, updateUser} =
    authSlice.actions;

export default authSlice.reducer;
