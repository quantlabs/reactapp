// React
import React from 'react';
import {View} from 'react-native';

// Styling
import styled from 'styled-components/native';

// Our Components
import ASpinner from '../components/ASpinner';
import ALogo from '../components/ALogo';
import {ViewContainerWithSpace} from '../components/Containers';
import {Text} from '../components/Typography';

const Container = styled(ViewContainerWithSpace)`
    text-align: 'center';
    justify-content: center;
    align-items: center;
`;

const LogoContainer = styled.View`
    padding-top: 100px;
`;

const Descriptor = styled(Text)`
    margin-vertical: 20px;
`;

export default function Splash() {
    return (
        <Container>
            <LogoContainer>
                <ALogo width={200} />
            </LogoContainer>
            <View>
                <Descriptor>Загружаем данные</Descriptor>
                <ASpinner />
            </View>
        </Container>
    );
}
