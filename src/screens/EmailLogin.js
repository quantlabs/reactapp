// React
import React from 'react';
import {View, Alert} from 'react-native';

// Redux
import {useDispatch, useSelector} from 'react-redux';

// Styling
import styled from 'styled-components/native';

// Components
import AButton from '../components/AButton';
import AInput from '../components/AInput';
import {ViewContainer} from '../components/Containers';

// Forms
import {Formik} from 'formik';
import {getToken} from '../features/auth/authApi';

const ViewContainerFlexEnd = styled(ViewContainer)`
    justify-content: flex-end;
`;

const validate = values => {
    const errors = {};
    if (!values.username) {
        errors.username = 'Введите email';
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.username)
    ) {
        errors.username = 'Некорректный адрес';
    }

    if (!values.password) {
        errors.password = 'Введите пароль';
    }

    return errors;
};

export function EmailLogin() {
    const auth = (values, setSubmitting) => {
        getToken(values).then(res => {
            if (res.error) {
                Alert.alert('Не удалось войти', res.error);
            }
        });
        setSubmitting(false);
    };

    return (
        <ViewContainerFlexEnd>
            <Formik
                initialValues={{
                    username: 'root@root.root',
                    password: 'root',
                }}
                validate={values => validate(values)}
                onSubmit={(values, {setSubmitting}) => {
                    auth(values, setSubmitting);
                }}>
                {({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    values,
                    isSubmitting,
                    errors,
                    touched,
                }) => (
                    <View>
                        <AInput
                            title={'Email'}
                            onChangeText={handleChange('username')}
                            onBlur={handleBlur('username')}
                            value={values.username}
                            autoFocus={true}
                            autoCorrect={false}
                            autoCapitalize={'none'}
                            returnKeyType={'next'}
                            textContentType={'emailAddress'}
                            autoCompleteType={'email'}
                            keyboardType={'email-address'}
                            error={
                                errors.username &&
                                touched.username &&
                                errors.username
                            }
                        />
                        <AInput
                            title={'Пароль'}
                            onChangeText={handleChange('password')}
                            onBlur={handleBlur('password')}
                            value={values.password}
                            autoCorrect={false}
                            autoCapitalize={'none'}
                            clearTextOnFocus
                            enablesReturnKeyAutomatically
                            textContentType={'password'}
                            autoCompleteType={'password'}
                            secureTextEntry={true}
                            error={
                                errors.password &&
                                touched.password &&
                                errors.password
                            }
                        />
                        <AButton
                            onPress={handleSubmit}
                            title="Войти"
                            disabled={isSubmitting}
                        />
                    </View>
                )}
            </Formik>
        </ViewContainerFlexEnd>
    );
}

export default EmailLogin;
