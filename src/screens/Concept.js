import React, {useEffect, useState} from 'react';
import {RefreshControl, View} from 'react-native';

// Navigation
import {useIsFocused} from '@react-navigation/native';

// Features
import {getConceptById} from '../features/subjects/subjectsApi';
import {getConceptStats} from '../features/stats/statsApi';
import {getConceptUtr} from '../features/tasks/tasksApi';

// Styling
import styled, {useTheme} from 'styled-components/native';

// Components
import {ScrollContainer, ViewContainer} from '../components/Containers';
import {TextH1, TextSmall, TextSmallMuted} from '../components/Typography';
import ABadge from '../components/ABadge';
import ASpinner from '../components/ASpinner';
import ATextWithLine from '../components/ATextWithLine';
import ACard from '../components/ACard';
import {RowWrapper, ColWrapper, ColDelimeter} from '../components/Layout';
import AButton from '../components/AButton';

const BadgeContainer = styled.View`
    align-items: flex-start;
    flex-direction: row;
    margin-top: 5px;
`;

const ConceptTitle = styled(TextH1)`
    margin-top: 10px;
`;

const StatsRow = styled(RowWrapper)`
    margin-top: 10px;
`;

const NodesRow = styled(RowWrapper)`
    margin-top: 5px;
`;

const CardText = styled(TextSmall)`
    color: ${props => props.theme.colors.bgColor};
`;

const ConceptButtonsContainer = styled.View`
    margin-top: 15px;
`;

const ConceptContainer = styled.View`
    flex: 1;
`;

const Concept = ({route, navigation}) => {
    const conceptTagId = route.params.conceptTagId;
    const [conceptTag, setConceptTag] = useState(null);

    const [refreshing, setRefreshing] = useState(false);

    const [conceptStats, setConceptStats] = useState();

    const theme = useTheme();
    const isFocused = useIsFocused();

    const getData = async () => {
        await getConceptById(conceptTagId)
            .then(concept => {
                if (isFocused) {
                    setConceptTag(concept);
                    return concept;
                }
            })
            .then(concept => {
                if (concept.is_solved) {
                    getConceptStats(conceptTagId).then(stats => {
                        if (isFocused) {
                            setConceptStats(stats);
                        }
                    });
                }
            });
    };

    const onSolveClick = () => {
        getConceptUtr(conceptTagId).then(utr =>
            navigation.navigate('Task', {utrId: utr.id}),
        );
    };

    // когда пользователь тянет вниз, обновляем список концептов и дерево экзаменов
    const onRefresh = () => {
        setRefreshing(true);
        getData().then(setRefreshing(false));
    };

    useEffect(() => {
        if (isFocused) {
            getData();
        }
    }, [isFocused]);

    // Собираем кнопку в зависимости от того решал или не решал, броса или не бросал пользователь тестирование
    const ConceptButtons = () => {
        // Если концепт не пройден в режиме диагностики, принимаем решение дальше
        if (!conceptTag.is_solved) {
            // Если полученный utr указывает на первый вопрос, то значит тестирование еще не начиналось
            if (conceptTag.first_time) {
                return (
                    <>
                        <TextSmallMuted>Тема еще не решалась</TextSmallMuted>
                        <AButton
                            title="Начать диагностику"
                            onPress={onSolveClick}
                        />
                    </>
                );
            } else {
                return (
                    <>
                        <TextSmallMuted>
                            Первая диагностика не завершена
                        </TextSmallMuted>
                        <AButton
                            title="Продолжить диагностику"
                            onPress={onSolveClick}
                        />
                    </>
                );
            }
        } else {
            // Если концепт пройден в режиме диагностики, принимаем решение дальше
            // Если прилетел массив с -1, то все задачи вырешаны и можно показать только результаты
            if (conceptTag.questions_are_over) {
                return (
                    <TextSmallMuted>Решены все задачи в теме</TextSmallMuted>
                );
            } else {
                return (
                    <>
                        <TextSmallMuted>
                            Можно продолжить отработку
                        </TextSmallMuted>
                        <AButton
                            title="Продолжить тестирование"
                            onPress={onSolveClick}
                        />
                    </>
                );
            }
        }
    };

    return (
        <ViewContainer>
            <ConceptContainer>
                {conceptTag ? (
                    <>
                        <BadgeContainer>
                            <ABadge
                                title={conceptTag.subject.subject.title}
                                variant={'pink'}
                            />
                        </BadgeContainer>
                        <ConceptTitle>{conceptTag.concept.title}</ConceptTitle>
                        {conceptStats && (
                            <ScrollContainer
                        showsVerticalScrollIndicator={false}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                                style={{tintColor: theme.colors.textColor}}
                            />
                        }>
                                <StatsRow>
                                    <ColWrapper>
                                        <ACard variant={'secondary'}>
                                            <TextSmall>Общий балл</TextSmall>
                                            <TextH1>
                                                {conceptStats.score} из{' '}
                                                {conceptStats.total}
                                            </TextH1>
                                        </ACard>
                                    </ColWrapper>
                                    <ColDelimeter />
                                    <ColWrapper>
                                        <ACard variant={'secondary'}>
                                            <TextSmall>Освоение темы</TextSmall>
                                            <TextH1>
                                                {conceptStats.percent}%
                                            </TextH1>
                                        </ACard>
                                    </ColWrapper>
                                </StatsRow>
                                <ATextWithLine title="Твои знания и навыки" />
                                <NodesRow>
                                    <ColWrapper>
                                        {conceptStats.best.map(node => (
                                            <ACard
                                                variant={'success'}
                                                key={node.id}>
                                                <CardText>
                                                    {node.title}
                                                </CardText>
                                            </ACard>
                                        ))}
                                    </ColWrapper>
                                    <ColDelimeter />
                                    <ColWrapper>
                                        {conceptStats.worst.map(node => (
                                            <ACard
                                                variant={'danger'}
                                                key={node.id}>
                                                <CardText>
                                                    {node.title}
                                                </CardText>
                                            </ACard>
                                        ))}
                                    </ColWrapper>
                                </NodesRow>
                            </ScrollContainer>
                        )}
                    </>
                ) : (
                    <ASpinner />
                )}
            </ConceptContainer>
            {conceptTag ? (
                <ConceptButtonsContainer>
                    <ConceptButtons />
                </ConceptButtonsContainer>
            ) : null}
        </ViewContainer>
    );
};

export default Concept;
