/**
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState, useLayoutEffect} from 'react';

// Redux
import {useSelector} from 'react-redux';

// Backend
import {getUserStats} from '../features/auth/authApi';

// Navigation
import {useIsFocused} from '@react-navigation/native';

// Styling
import styled from 'styled-components/native';

// Components
import {ScrollContainer, ViewContainer} from '../components/Containers';
import {Text, TextH1, TextH1Bold, TextSmall} from '../components/Typography';
import ATextWithLine from '../components/ATextWithLine';
import ACard from '../components/ACard';
import {HeaderAvatar, HeaderBack} from '../components/Header';
import ALogo from '../components/ALogo';
import {levelTitles} from '../features/stats/levelTitles';
import AProgressBar from '../components/AProgressBar';
import ASpinner from '../components/ASpinner';

const HelloText = styled(TextH1)`
    margin-top: 15px;
`;

const UserStatsContainer = styled.View`
    margin-top: 30px;
    margin-bottom: 20px;
`;

const Dashboard = ({navigation}) => {
    const auth = useSelector(store => store.auth);
    const subjectTags = useSelector(state => state.subjects.all);

    const [userLevel, setUserLevel] = useState();
    const [progressScore, setProgressScore] = useState();

    const isFocused = useIsFocused();

    const loadStats = () => {
        getUserStats().then(totalSolved => {
            let solved_level = Math.ceil((totalSolved + 1) / 30) - 1;
            setUserLevel(solved_level);

            let progress_score = totalSolved - solved_level * 30;
            setProgressScore(progress_score);
        });
    };

    // Get fresh stats on screen focus
    useEffect(() => {
        if (isFocused) {
            loadStats();
        }
    }, [isFocused]);

    // Change navigation view
    useLayoutEffect(() => {
        if (isFocused) {
            navigation.setOptions({
                headerLeft: () => (
                    <HeaderAvatar
                        onPress={() => navigation.navigate('Profile')}
                    />
                ),
                headerTitle: () => false,
                headerRight: () => <ALogo size={100} />,
            });
        }
    }, [navigation, isFocused]);

    return (
        <ViewContainer>
            <HelloText>
                Привет,
                <TextH1Bold>
                    {' '}
                    {auth.user.first_name ? auth.user.first_name : 'Анонимус'}!
                </TextH1Bold>
            </HelloText>
            {userLevel !== null ? (
                <UserStatsContainer>
                    <Text>
                        Твой уровень:{' '}
                        {userLevel && userLevel < levelTitles.length
                            ? levelTitles[userLevel]
                            : levelTitles[levelTitles.length - 1]}{' '}
                        {userLevel && `(${userLevel + 1})`}
                    </Text>
                    <AProgressBar
                        value={((progressScore + 3) / 33) * 100}
                        variant={'main'}
                    />
                    <TextSmall>
                        Задач до следующего уровня:&nbsp;
                        {30 - progressScore}
                    </TextSmall>
                </UserStatsContainer>
            ) : (
                <ASpinner size="large" />
            )}
            {subjectTags && (
                <>
                    <ATextWithLine title="Выбор предмета" />
                    {subjectTags.map(subjectTag => (
                        <ACard
                            title={subjectTag.title}
                            key={subjectTag.id}
                            onPress={() => {
                                navigation.navigate('Subject', {
                                    subjectTagId: subjectTag.id,
                                });
                            }}
                        />
                    ))}
                </>
            )}
        </ViewContainer>
    );
};

export default Dashboard;
