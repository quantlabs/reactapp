export const LOGO_GRAY = require('./logos/LogoGray.png');
export const LOGO_WHITE = require('./logos/LogoWhite.png');

export const EMPTY_AVATAR = require('./avatars/empty_avatar.jpg');
