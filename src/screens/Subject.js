// React
import React, {useEffect, useLayoutEffect, useRef, useState} from 'react';
import {RefreshControl, FlatList} from 'react-native';

// Redux
import {useDispatch, useSelector} from 'react-redux';

// Backend
import {getConceptsBySubjectTagId} from '../features/subjects/subjectsApi';
import {getExamTree} from '../features/examTree/examTreeApi';
import {getConceptsStats} from '../features/stats/statsApi';

// Styling
import styled, {useTheme} from 'styled-components/native';

// Navigation
import {useIsFocused} from '@react-navigation/native';

// Our Components
import {ViewContainer, ScrollContainer} from '../components/Containers';
import ATextWithLine from '../components/ATextWithLine';
import ACard from '../components/ACard';
import AButton from '../components/AButton';
import AProgressBar from '../components/AProgressBar';
import ABadge from '../components/ABadge';
import ALogo from '../components/ALogo';

const HorizontalMenuContainer = styled.View`
    margin-top: 5px;
    padding-vertical: 10px;
    padding-horizontal: 0px;
    background-color: ${props => {
        if (props.theme.isDark) {
            return props.theme.const.Colors.darkGrayAccent;
        } else {
            return props.theme.const.Colors.brightGrayAccent;
        }
    }};
`;

// Формируем список экзаменов, доступных для исполнения
// Нужен массив [{title: 'ЕГЭ'}, {}]
const filterExamsBySubjectTagId = (examTree, subjectTagId) => {
    let examTitlesForThisSubject = [];
    for (let exam of examTree.exams) {
        for (let subject of exam.subjects) {
            if (subject.subject_tag_id === subjectTagId) {
                examTitlesForThisSubject.push({
                    title: exam.title,
                    examTagId: exam.exam_tag_id,
                    // subject: {
                    //     title: subject.title,
                    //     subjectTagId: subject.subject_tag_id,
                    // },
                    // nums: subject.nums,
                });
            }
        }
    }
    return examTitlesForThisSubject;
};

const ConceptCard = ({
    conceptTag,
    subtitle,
    subjectTagId,
    navigation,
    variant,
    ...props
}) => {
    return (
        <ACard
            title={conceptTag.concept.title}
            subtitle={subtitle}
            variant={variant}
            onPress={() => {
                navigation.navigate('Concept', {
                    conceptTagId: conceptTag.id,
                });
            }}>
            {props.children}
        </ACard>
    );
};

export const Subject = ({navigation, route}) => {
    const [subjectTagId, setSubjectTagId] = useState(route.params.subjectTagId);
    const subjectTags = useSelector(state => state.subjects.all);
    const subjectTag = subjectTags.find(x => x.id === subjectTagId);

    const newConcepts = subjectTag.concepts
        ? subjectTag.concepts.filter(s => !s.is_solved && s.is_active)
        : false;
    const solvedConcepts = subjectTag.concepts
        ? subjectTag.concepts.filter(s => s.is_solved && s.is_active)
        : false;
    const subjectStats = subjectTag.stats || false;

    const examTree = useSelector(store => store.examTree.tree);
    const examsList = filterExamsBySubjectTagId(examTree, subjectTagId);

    const [refreshing, setRefreshing] = useState(false);

    const isFocused = useIsFocused();
    const dispatch = useDispatch();

    const theme = useTheme();

    const flatlistRef = useRef();

    // во время первого запуска загружаем список концептов и сохраняем в redux
    // даем возможность загрузить концепты заново, потянув вниз

    // на каждое возвращение к экрану загружаем статистику и матчим их вместе, генерируя массив для отрисовки

    // когда пользователь тянет вниз, обновляем список концептов и дерево экзаменов
    const onRefresh = () => {
        setRefreshing(true);
        Promise.all([
            getExamTree(),
            getConceptsBySubjectTagId(subjectTagId),
            getConceptsStats(subjectTagId),
        ]).then(() => {
            setRefreshing(false);
        });
    };

    // Change navigation view
    useLayoutEffect(() => {
        if (isFocused) {
            navigation.setOptions({
                title: subjectTag.title,
                headerTitle: () => <ALogo size={100} />,
            });
        }
    }, [navigation, isFocused]);

    useEffect(() => {
        if (isFocused) {
            if (!subjectTag.concepts) {
                getConceptsBySubjectTagId(subjectTagId);
            }
            if (!subjectTag.stats) {
                getConceptsStats(subjectTagId);
            }
        }
    }, [isFocused, subjectTagId]);

    return (
        <>
            <HorizontalMenuContainer>
                <FlatList
                    data={subjectTags}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={item => item.id.toString()}
                    ref={flatlistRef}
                    renderItem={e => {
                        return (
                            <ABadge
                                title={e.item.title}
                                variant={
                                    e.item.id === subjectTagId ? 'pink' : false
                                }
                                customStyles={
                                    e.index === 0
                                        ? {marginLeft: 15}
                                        : e.index === subjectTags.length - 1
                                        ? {marginRight: 15}
                                        : {}
                                }
                                onPress={() => {
                                    setSubjectTagId(e.item.id);
                                    flatlistRef.current.scrollToIndex({
                                        animated: true,
                                        index: e.index,
                                    });
                                }}
                            />
                        );
                    }}
                />
            </HorizontalMenuContainer>
            <ViewContainer>
                <ScrollContainer
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                            style={{tintColor: theme.colors.textColor}}
                        />
                    }>
                    {examsList.length > 0 && (
                        <>
                            <ATextWithLine title="Подготовка к экзаменам" />
                            {examsList.map(exam => (
                                <AButton
                                    variant={'pink'}
                                    title={`Тренажер ${exam.title}`}
                                    key={exam.examTagId}
                                    onPress={() => {
                                        navigation.navigate('Trainer', {
                                            subjectTagId: subjectTagId,
                                            examTagId: exam.examTagId,
                                        });
                                    }}
                                />
                            ))}
                        </>
                    )}
                    {newConcepts.length > 0 && (
                        <>
                            <ATextWithLine title="Доступные темы" />
                            {newConcepts.map(conceptTag => (
                                <ConceptCard
                                    variant={'default'}
                                    key={conceptTag.id}
                                    navigation={navigation}
                                    conceptTag={conceptTag}
                                    subjectTagId={subjectTagId}
                                />
                            ))}
                        </>
                    )}
                    {solvedConcepts.length > 0 && (
                        <>
                            <ATextWithLine title="Результаты" />
                            {solvedConcepts.map(conceptTag => (
                                <ConceptCard
                                    variant={'secondary'}
                                    subtitle={`${
                                        subjectStats[conceptTag.id.toString()]
                                    }%`}
                                    navigation={navigation}
                                    key={conceptTag.id}
                                    conceptTag={conceptTag}
                                    subjectTagId={subjectTagId}>
                                    <AProgressBar
                                        value={
                                            subjectStats[
                                                conceptTag.id.toString()
                                            ]
                                        }
                                    />
                                </ConceptCard>
                            ))}
                        </>
                    )}
                </ScrollContainer>
            </ViewContainer>
        </>
    );
};

export default Subject;
