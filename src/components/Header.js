// React
import React from 'react';
import {TouchableOpacity, View} from 'react-native';

// Redux
import {useSelector} from 'react-redux';

// Images
import Svg, {Path} from 'react-native-svg';

// Styling
import styled, {useTheme} from 'styled-components/native';

// Components
import {UserAvatar} from './Avatar';
import {TextSmall} from './Typography';

const AvatarOpacity = styled.TouchableOpacity`
    padding-left: 0px;
    flex-direction: row;
    display: flex;
`;

const HeaderAvatarName = styled(TextSmall)`
    line-height: 40px;
    padding-left: 10px;
`;

export const HeaderAvatar = ({onPress}) => {
    const user = useSelector(state => state.auth.user);

    return (
        <AvatarOpacity onPress={onPress}>
            <UserAvatar size={40} />
            {user.first_name && (
                <HeaderAvatarName>{user.first_name}</HeaderAvatarName>
            )}
        </AvatarOpacity>
    );
};

const BackButtonView = styled.View`
    align-self: center;
    padding-right: 15px;
    padding-vertical: 10px;
    background-color: 'red';
`;

export const HeaderBackButton = () => {
    const theme = useTheme();
    return (
        <Svg width="11" height="18" viewBox="0 0 11 18" fill="none">
            <Path
                d="M11 2.57143L4.125 9L11 15.4286L9.625 18L0 9L9.625 0L11 2.57143Z"
                fill={theme.colors.textColor}
            />
        </Svg>
    );
};

export const HeaderBack = ({onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <BackButtonView>
                <HeaderBackButton></HeaderBackButton>
            </BackButtonView>
        </TouchableOpacity>
    );
};
