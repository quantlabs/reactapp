import fetchClient, {handleError} from '../../backend/http';
import {setExamTree} from './examTreeSlice';
import {store} from '../../redux';

export const getExamTree = async () => {
    await fetchClient
        .get('tags/tree/')
        .then(res => {
            store.dispatch(setExamTree(res.data));
        })
        .catch(error => {
            handleError(error, 'fetch exam tree error');
        });
};
