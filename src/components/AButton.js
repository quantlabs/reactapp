// React
import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

// Styling
import styled from 'styled-components/native';

// Constants
const buttonsDefaults = {
    height: 50,
    borderRadius: 5,
    marginTop: 10,
    fontSize: 18,
};

const ButtonContainer = styled.TouchableOpacity`
    margin-top: 10px;
`;

const Button = styled.Text`
    height: 50px;
    line-height: 50px;
    border-radius: 5px;
    font-size: 18px;
    text-align: center;
    text-align-vertical: center;
    overflow: hidden;
    color: ${props => props.theme.const.Colors.white};
    font-family: ${props => props.theme.const.Fonts.family.main};
    background-color: ${props => {
        if (props.disabled) {
            return props.theme.const.Colors.darkGrayAccent;
        } else {
            switch (props.variant) {
                case 'default':
                    return props.theme.const.Colors.blueAccent;
                case 'pink':
                    return props.theme.const.Colors.pinkAccent;
                case 'secondary':
                    return props.theme.const.Colors.darkGrayAccent;
                case 'danger':
                    return props.theme.const.Colors.redMessage50;
                default:
                    return props.theme.const.Colors.blueAccent;
            }
        }
    }};
`;

// button component
export default function AButton({
    title,
    onPress,
    disabled,
    variant = 'default',
    ...props
}) {
    return (
        <ButtonContainer onPress={onPress} disabled={disabled}>
            <Button disabled={disabled} variant={variant}>
                {props.children ?? title}
            </Button>
        </ButtonContainer>
    );
}
