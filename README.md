# React Native Apps

## Run Application locally

Step 1. Run Metro

```yarn react-native start```

> If you're familiar with web development, Metro is a lot like webpack—for React Native apps. Unlike Swift or Objective-C, JavaScript isn't compiled—and neither is React Native. Bundling isn't the same as compiling, but it can help improve startup performance and translate some platform-specific JavaScript into more widely supported JavaScript.

Step 2. Start your application

Run instructions for Android:
• Have an Android emulator running (quickest way to get started), or a device connected. • npx react-native run-android

Run instructions for iOS:
• npx react-native run-ios

- or - • Open ReactApp/ios/ReactApp.xcworkspace in Xcode or run "xed -b ios"
  • Hit the Run button

## Install custom libraries

[Database of libraries](https://reactnative.directory)

Step 1. Install library

```yarn add react-native-webview```

Step 2. Link native code to iOS

```npx pod-install```

Step 3. Re-build the app binary to start using your new library

```npx react-native run-ios```

```npx react-native run-android```

## Architecture:

- [x] Use Prettier linter for code beautify

- [x] Define style library and theming strategy
  🔥 [How to Add Support for Dark and Light Themes in React Native ](https://www.crowdbotics.com/blog/how-to-add-support-for-dark-and-light-themes-in-react-native-apps)
  with styled-components and Context

- [x] Use Redux Toolkit for easy implementation of Redux to store and update data

- [x] Use redux-thunk for extend the store's abilities, and lets you write async logic that interacts with the store

- [x] Sentry bug reporting

- [x] Use redux-persist for saving store locally
- 
- [x] Custom fonts [Article](https://mehrankhandev.medium.com/ultimate-guide-to-use-custom-fonts-in-react-native-77fcdf859cf4)

- [x] User @react-navigation for move between screens

- [x] Add dark theme support and user settings with Redux

- [x] Use formik and for form building&validation

- [x] Add ImagePicker with [react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker)

- [x] Use KeyBoardAvoidingView correctly

- [x] Create correct fetch process [Article](https://blog.logrocket.com/patterns-for-data-fetching-in-react-981ced7e5c56/)

- [x] Get token from server by email

- [x] Show splash while data is loading

- [x] Build on Android

- [x] Add app identifiers, change from ReactApp, [Package](https://www.npmjs.com/package/react-native-rename)

- [x] Render MAth with Katex and Markdown correctly

1. [react-native-math-view](https://github.com/ShaMan123/react-native-math-view)
2. [react-native-markdown-display](https://github.com/iamacup/react-native-markdown-display)
3. Article [Adding KaTeX and Markdown in React](https://levelup.gitconnected.com/adding-katex-and-markdown-in-react-7b70694004ef)
4. Alternative [react-native-easy-markdown](https://github.com/TitanInvest/react-native-easy-markdown)

- [x] Add favicons ([Library for generate icons from one svg](https://github.com/aeirola/react-native-svg-app-icon))

- [x] Set Onboarding Screens

- [ ] Send updated user avatar to backend in Profile.js

1. Send without 'content-type' leads to "Can't save file in database Неподдерживаемый тип данных \"application/json\" в запросе."
2. Send with 'multipart/form-data' leads to "Can't save file in database Multipart form parse error - Invalid boundary in multipart: None"

- [ ] Add Apple auth on ios

- [ ] Add VK native app auth

## Questions for mentor

- [ ] How to post file (boundary & FormData.getHeaders() is ont a function error)?

- [ ] How to add react-native-vkontakte-login?

## Errors

Error 1. Issues then `react-native build-ios`
```
Stop Metro server (optionally)
rm -rf ~/Library/Caches/CocoaPods (optional)
rm -rf ios/Pods
rm -rf ~/Library/Developer/Xcode/DerivedData/*
pod deintegrate (optional)
pod setup (optional)
react-native start --reset-cache (optional)
watchman watch-del-all (optional)
npx pod-install
npx react-native run-ios
```
