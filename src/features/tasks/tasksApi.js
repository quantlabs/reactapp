import fetchClient, {handleError} from '../../backend/http';

export const getUtr = async utrId => {
    let result = await fetchClient
        .get(`utrs/${utrId}/`)
        .then(res => res.data)
        .catch(error => {
            handleError(error, 'get utr error');
        });
    return result;
};

export const getThemeUtr = async themeId => {
    let result = await fetchClient
        .get(`theme_task/${themeId}/`)
        .then(res => res.data)
        .catch(error => {
            handleError(error, 'get theme utr error');
        });
    return result;
};

export const getExamSubjectNumUtr = async (examTagId, subjectTagId, num) => {
    let result = await fetchClient
        .post(`num_task/`, {
            subject: subjectTagId,
            exam: examTagId,
            num: num,
        })
        .then(res => res.data)
        .catch(error => {
            handleError(error, 'get exam subject num utr error');
        });
    return result;
};

export const getExamSubjectUtr = async (examTagId, subjectTagId) => {
    let result = await fetchClient
        .get(`stat/exam/?exam=${examTagId}&subject=${subjectTagId}`)
        .then(res => res.data)
        .catch(error => {
            handleError(error, 'get exam subject utr error');
        });
    return result;
};

export const getConceptUtr = async conceptTagId => {
    let result = await fetchClient
        .get(`concept_task/${conceptTagId}/`)
        .then(res => res.data)
        .catch(error => {
            handleError(error, 'get concept utr error');
        });
    return result;
};

export const finishUtr = async utrId => {
    let result = await fetchClient
        .get(`utrs/${utrId}/finish/`)
        .then(res => res.data)
        .catch(error => {
            handleError(error, 'finish utr error');
        });
    return result;
};
