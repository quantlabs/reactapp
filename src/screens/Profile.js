// React
import React, {useState} from 'react';
import {Switch, View, TouchableOpacity, Image, Alert} from 'react-native';

// Redux
import {useDispatch, useSelector} from 'react-redux';
import {logout} from '../features/auth/authSlice';
import {setDarkMode} from '../features/settings/settingsSlice';

// Features
import {postUserAvatar, postUserUpdates} from '../features/auth/authApi';

// Styling
import styled, {useTheme} from 'styled-components/native';

// Forms
import {Formik} from 'formik';
import ImagePicker from 'react-native-image-crop-picker';

// Our Components
import {ScrollContainer, ViewContainer} from '../components/Containers';
import {TextH2} from '../components/Typography';
import AButton from '../components/AButton';
import {AInput, InputTitle} from '../components/AInput';
import {UserAvatar} from '../components/Avatar';
import APicker from '../components/APicker';

const FieldTitle = styled(InputTitle)`
    margin-top: 15px;
`;

const NewAvatar = styled(Image)`
    border-radius: 5px;
    width: ${props => props.size}px;
    height: ${props => props.size}px;
    overflow: hidden;
`;

const Profile = ({navigation}) => {
    const settings = useSelector(store => store.settings);
    const user = useSelector(store => store.auth.user);
    const dispatch = useDispatch();
    const theme = useTheme();

    const [newAvatar, setNewAvatar] = useState();

    const [gradeDropdownOpen, setGradeDropdownOpen] = useState(false);

    return (
        <ViewContainer>
            <ScrollContainer showsVerticalScrollIndicator={false}>
                <TextH2>Редактировать данные</TextH2>
                {user && (
                    <Formik
                        initialValues={{
                            first_name: user.first_name,
                            last_name: user.last_name,
                            grade: user.grade ? user.grade.toString() : null,
                        }}
                        onSubmit={(values, {setSubmitting}) => {
                            postUserUpdates(values).then(() => {
                                setSubmitting(false);
                                navigation.navigate('Dashboard');
                            });
                            if (newAvatar) {
                                postUserAvatar(newAvatar, user);
                            }
                        }}>
                        {({
                            handleChange,
                            setFieldValue,
                            handleSubmit,
                            values,
                            isSubmitting,
                            errors,
                            touched,
                        }) => (
                            <View>
                                <FieldTitle>Аватар</FieldTitle>
                                <TouchableOpacity
                                    onPress={() => {
                                        Alert.alert(
                                            'Новая аватарка',
                                            'Откуда возьмем?',
                                            [
                                                {
                                                    text: 'Выбрать фото',
                                                    onPress: () => {
                                                        ImagePicker.openPicker({
                                                            width: 400,
                                                            height: 400,
                                                            forceJpg: true,
                                                            cropping: true,
                                                        }).then(image => {
                                                            setNewAvatar(image);
                                                        });
                                                    },
                                                },
                                                {
                                                    text: 'Камера',
                                                    onPress: () => {
                                                        ImagePicker.openCamera({
                                                            width: 400,
                                                            height: 400,
                                                            forceJpg: true,
                                                            cropping: true,
                                                        }).then(image => {
                                                            console.log(image);
                                                            setNewAvatar(image);
                                                        });
                                                    },
                                                },
                                                {
                                                    text: 'Отмена',
                                                    style: 'cancel',
                                                },
                                            ],
                                            {cancelable: true},
                                        );
                                    }}>
                                    {newAvatar ? (
                                        <NewAvatar
                                            size={200}
                                            source={{
                                                uri: newAvatar.sourceURL,
                                            }}
                                        />
                                    ) : (
                                        <UserAvatar size={200} />
                                    )}
                                </TouchableOpacity>
                                <AInput
                                    title={'Имя'}
                                    onChangeText={handleChange('first_name')}
                                    value={values.first_name}
                                    autoCorrect={false}
                                    error={
                                        errors.first_name &&
                                        touched.first_name &&
                                        errors.first_name
                                    }
                                />
                                <AInput
                                    title={'Фамилия'}
                                    onChangeText={handleChange('last_name')}
                                    value={values.last_name}
                                    autoCorrect={false}
                                    error={
                                        errors.last_name &&
                                        touched.last_name &&
                                        errors.last_name
                                    }
                                />
                                <APicker
                                    title={'Класс'}
                                    onValueChange={value =>
                                        setFieldValue('grade', value)
                                    }
                                    value={values.grade}
                                    placeholder={{
                                        label: 'Не выбран...',
                                        value: null,
                                    }}
                                    items={[
                                        {label: '11 класс', value: '11'},
                                        {label: '10 класс', value: '10'},
                                        {label: '9 класс', value: '9'},
                                        {label: '8 класс', value: '8'},
                                        {label: '7 класс', value: '7'},
                                        {label: '6 класс', value: '6'},
                                    ]}
                                />
                                <AButton
                                    onPress={handleSubmit}
                                    title="Сохранить"
                                    disabled={isSubmitting}
                                />
                            </View>
                        )}
                    </Formik>
                )}
                <AButton title={'Выйти'} onPress={() => dispatch(logout())} />
                <FieldTitle>Темная тема</FieldTitle>
                <Switch
                    style={{alignSelf: 'flex-start'}}
                    value={settings.darkMode}
                    onValueChange={value => dispatch(setDarkMode(value))}
                />
            </ScrollContainer>
        </ViewContainer>
    );
};

export default Profile;
