import fetchClient, {
    fetchWithDataClient,
    handleError,
} from '../../backend/http';
import {store} from '../../redux';
import {logout, setToken, setUser, updateUser} from './authSlice';
import FormData from 'form-data';
import {Platform} from 'react-native';

export const checkAuth = async () => {
    let result = await fetchClient
        .get('check/')
        .then(res => true)
        .catch(error => {
            store.dispatch(logout());
            console.log('Check token failed');
            return false;
        });
    return result;
};

export const getToken = async values => {
    let result = await fetchClient
        .post('token-auth/', values)
        .then(res => {
            if (res.data.token) {
                store.dispatch(setToken(res.data.token));
                return true;
            } else {
                return false;
            }
        })
        .catch(error => {
            handleError(error, 'get token');
            if (error.response.data.non_field_errors) {
                return {
                    error: JSON.stringify(
                        error.response.data.non_field_errors[0],
                    ),
                };
            }
        });
    return result;
};

export const getUser = async () => {
    let result = await fetchClient
        .get('user/')
        .then(res => {
            store.dispatch(setUser(res.data));
            return res.data;
        })
        .catch(error => {
            handleError(error, 'load user data error');
            return false;
        });
    return result;
};

export const postUserAvatar = async (avatar, user) => {
    console.log('Arguments', avatar, user);

    const data = new FormData();
    data.append('upload', {
        name: `avatar_${user.email}_${avatar.filename}`,
        type: avatar.mime,
        uri:
            Platform.OS === 'ios'
                ? avatar.sourceURL.replace('file://', '')
                : avatar.sourceURL,
    });

    console.log('FormData', data, data.getHeaders());

    let result = await fetchWithDataClient
        .post('user/avatar/', data)
        .then(res => {
            console.log(res);
        })
        .catch(error => {
            handleError(error, 'update user avatar error');
            return false;
        });
    return result;
};

export const postUserUpdates = async values => {
    let result = await fetchClient
        .post('user/', values)
        .then(() => {
            store.dispatch(updateUser(values));
        })
        .catch(error => {
            handleError(error, 'update user data error');
            return false;
        });
    return result;
};

export const getUserStats = async () => {
    let result = await fetchClient
        .get('stat/solved/')
        .then(res => {
            return res.data.solved_questions;
        })
        .catch(error => {
            handleError(error, 'get stats error');
        });
    return result;
};
